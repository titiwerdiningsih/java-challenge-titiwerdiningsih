package ist.challenge.titi_werdiningsih;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TitiWerdiningsihApplication {

	public static void main(String[] args) {
		SpringApplication.run(TitiWerdiningsihApplication.class, args);
	}

}
