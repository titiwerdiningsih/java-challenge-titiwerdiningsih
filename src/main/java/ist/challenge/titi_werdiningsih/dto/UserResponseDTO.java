package ist.challenge.titi_werdiningsih.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponseDTO {
    private Long userId;
    private String username;

    @Override
    public String toString() {
        return "UserResponseDTO{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                '}';
    }
}



