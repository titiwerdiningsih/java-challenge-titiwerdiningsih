package ist.challenge.titi_werdiningsih.entity;

import ist.challenge.titi_werdiningsih.dto.UserResponseDTO;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "users")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    private String username;
    private String password;

    public UserResponseDTO convertToResponse(){
        return UserResponseDTO.builder()
                .userId(this.userId)
                .username(this.username)
                .build();
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

