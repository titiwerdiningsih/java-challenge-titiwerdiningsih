package ist.challenge.titi_werdiningsih.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import ist.challenge.titi_werdiningsih.dto.UserRequestDTO;
import ist.challenge.titi_werdiningsih.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "User Controller")
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {
    UserService userService;

    @PostMapping("/signUp")
    public ResponseEntity<Object> register(@RequestBody UserRequestDTO userRequestDTO){
        return userService.register(userRequestDTO);
    }

    @PostMapping("/signIn")
    public ResponseEntity<Object> loginUser(@RequestBody UserRequestDTO userRequestDTO){
        return userService.login(userRequestDTO);
    }

    @PutMapping("/update/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable Long userId, @RequestBody UserRequestDTO userRequestDTO){
        return userService.updateUser(userId, userRequestDTO);
    }

    @GetMapping("/listUser")
    public ResponseEntity <Object> getAllUser(){
        return userService.getAllUsers();
    }

}

