package ist.challenge.titi_werdiningsih.service;

import ist.challenge.titi_werdiningsih.dto.UserRequestDTO;
import org.springframework.http.ResponseEntity;

public interface UserService {

    ResponseEntity<Object> getAllUsers();

    ResponseEntity<Object> register(UserRequestDTO userRequestDTO);

    ResponseEntity<Object> login(UserRequestDTO userRequestDTO);

    ResponseEntity<Object> updateUser(Long UserId, UserRequestDTO userRequestDTO);

}


